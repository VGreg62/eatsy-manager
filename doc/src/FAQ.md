# F.A.Q

## Présentation
### Qui sommes-nous ?
Nous sommes une équipe composée de trois étudiants en 4ème année de cycle Ingénieur à l’EPSI Lille ; Alessandro CECOTTI, Alexandre HALLEUX et Grégory VIANDIER.

### Pourquoi ce projet ?
Eatsy Manager a été réalisé dans le cadre d’un projet libre d’école pendant l’année scolaire 2019/2020.

### Pourquoi Eatsy Manager ?
Eatsy Manager est une application métier permettant aux restaurateur une assistance fonctionnelle. Elle est constituée de deux applications ; un menu dématérialisé avec deux types d’interfaces (serveur et utilisateur) relié à celle de réception de commande pour la cuisine et une application permettant la gestion du menu, sa personnalisation et l’aperçu de statistiques.
‘Eatsy Manager’ vient de l’association entre ‘Eat’ et ‘Easy’ pour ‘Manger facilement’. L’extension ‘Manager’ a été ajouté pour spécifier son importance métier afin de ne pas le confondre avec ‘Eatsy’ ; un projet développé parallèlement par une autre équipe orienté utilisateur mobile.

### Pourquoi choisir Eatsy Manager ?
 > On est les plus forts !!! Quoi de plus fort qu’un belge, un breton et un pas de calaisien !!!

### Comment se connecter pour la première fois ?

### Je n’arrive pas à me connecter ?

### Comment se connecter à la base de données ?

### Qui peut obtenir Eatsy ?
Eatsy est destiné à tous les restaurants, ceux a emporté, à volonté, gastronomique, petits boui-boui, peu importe le restaurant Eatsy vous aide à rendre votre restaurant plus performant.

### Dois-je changer/acheter mon matériel pour utiliser Eatsy ? 

### Faut-il plusieurs comptes pour utiliser Eatsy ?

### Faut-il télécharger l’application pour utiliser Eatsy ?

### Comment créer une carte de menu ?

### Comment ajouter une image dans le menu ?

### Comment utiliser l’application sur les tablettes serveurs ?

### Que faire si j’ai … comme erreur ?

### Comment créer des serveurs dans l’application ?

## Erreur de saisie
### Que faire si je me suis trompé dans la saisie de la carte ?

### Que faire si je ma suis trompé dans la saisie de la commande ?

## Modification de l’interface
### Comment changer la couleur de l’interface ?

### Comment changer les images de l’interface ?



