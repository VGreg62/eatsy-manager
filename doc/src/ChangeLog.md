# Changelog

Présentation du projet

## [Unrealeased]
### Added

### Changed

### Removed

## [1.0.2] 2020-01-11
### Added

- Add Process.CreateJson.java
- Add Process.CreateJsonDto.java
- Add Process.ManageMenu.java
- Add Process.ManageOrder.java
- Add Process.ManagePerson.java
- Add Process.ManageProduct.java
- Add Process.ManageTeam.java
- Add Process.OrderTreatment.java
- Add Process.ReadJson.java
- Add Process.ReadJsonDto.java

### Changed

- Change Changelog.md

## [1.0.1] 2019-12-19
### Added

- Add Dto.PersonDto.java
- Add Dto.MenuDto.java
- Add Dto.OrderDto.java
- Add Dto.ProductDto.java
- Add Dto.TeamDto.java
- Add Enumerator Models.Category.java
- Add Enumerator Models.Role.java
- Add Models.Menu.java
- Add Models.Order.java
- Add Models.Person.java
- Add Models.Product.java
- Add Models.Team.java
- Add Utils.Comparator.java
- Add Utils.Hash.java
- Add Utils.Mapper.java
- Add Utils.MapperDto.java
- Add Utils.StringUtils.java

### Changed

- Change Changelog.md

### Removed

- Add Enumerator Models.TeamName.java

## [1.0.0] 2019-11-19
### Added

- Create project
- Create Package
- Create Unit Test
- Create Contributing.md
- Create Changelog.md
- Add Enumerator Models.TeamName.java