package Dto;

/***
 * @author Alexandre Halleux
 * @version 1.0.1
 */

import java.util.ArrayList;

public class MenuDto {
    private int id;
    private String name;
    private ArrayList<ProductDto> listProduct;
    private double price;
    private double priceOffer;
    private boolean specialOffer;

    /***
     * No parametric constructor
     */
    public MenuDto(){
        listProduct = new ArrayList<>();
    }

    /***
     * Parametric constructor
     * @param id
     * @param name
     * @param listProduct
     * @param price
     */
    public MenuDto(int id, String name, ArrayList<ProductDto> listProduct, double price, double priceOffer, boolean specialOffer) {
        this.id = id;
        this.name = name;
        this.listProduct = listProduct;
        this.price = price;
        this.priceOffer = priceOffer;
        this.specialOffer = specialOffer;
    }

    /***
     * Getter id menuDto
     * @return id menuDto
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id menuDto
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter name menuDto
     * @return name menuDto
     */
    public String getName() {
        return name;
    }

    /***
     * Setter name menuDto
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * Getter ListProduct in menuDto
     * @return ListProduct in menuDto
     */
    public ArrayList<ProductDto> getListProduct() {
        return listProduct;
    }

    /***
     * Setter ListProduct in menuDto
     * @param listProduct
     */
    public void setListProduct(ArrayList<ProductDto> listProduct) {
        this.listProduct = listProduct;
    }

    /***
     * Getter price menuDto
     * @return price menuDto
     */
    public double getPrice() {
        return price;
    }

    /***
     * Setter price menuDto
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /***
     * Getter priceOffer menuDto
     * @return priceOffer menuDto
     */
    public double getPriceOffer() {
        return priceOffer;
    }

    /***
     * Setter priceOffer menuDto
     * @param priceOffer
     */
    public void setPriceOffer(double priceOffer) {
        this.priceOffer = priceOffer;
    }

    /***
     * Getter specialOffer menuDto
     * @return specialOffer menuDto
     */
    public boolean isSpecialOffer() {
        return specialOffer;
    }

    /***
     * Setter specialOffer menuDto
     * @param specialOffer
     */
    public void setSpecialOffer(boolean specialOffer) {
        this.specialOffer = specialOffer;
    }

}
