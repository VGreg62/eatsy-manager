package Dto;

/***
 * @author Alexandre Halleux
 * @version 1.0.1
 */

import Utils.Hash;

/***
 * @author Grégory Viandier
 * @version 1.0
 */

public class PersonDto {

    private int id;
    private String lastname;
    private String firstname;
    private String role;
    private String login;
    private String password;
    private boolean active;

    /***
     * No parametric constructor
     */
    public PersonDto(){}

    /***
     * Parametric Constructor
     * @param id
     * @param lastname
     * @param firstname
     * @param role
     * @param login
     * @param password
     * @param active
     */
    public PersonDto(int id, String lastname, String firstname, String role, String login, String password, boolean active) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.role = role;
        this.login = login;
        this.password = Hash.hashPassword(password);
        this.active = active;
    }

    /***
     * Getter id personDto
     * @return id personDto
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id personDto
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter lastname personDto
     * @return lastname personDto
     */
    public String getLastname() {
        return lastname;
    }

    /***
     * Setter lastname personDto
     * @param lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /***
     * Getter firstname personDto
     * @return firstname personDto
     */
    public String getFirstname() {
        return firstname;
    }

    /***
     * Setter firstname personDto
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /***
     * Getter role personDto
     * @return role personDto
     */
    public String getRole() {
        return role;
    }

    /***
     * Setter role personDto
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /***
     * Getter login personDto
     * @return login personDto
     */
    public String getLogin() {
        return login;
    }

    /***
     * Setter login personDto
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /***
     * Getter password personDto
     * @return password personDto
     */
    public String getPassword() {
        return password;
    }

    /***
     * Setter password personDto
     * @param password
     */
    public void setPassword(String password) {
        this.password = Hash.hashPassword(password);
    }

    /***
     * Getter active personDto
     * @return active personDto
     */
    public boolean isActive() {
        return active;
    }

    /***
     * Setter active personDto
     * @param active
     */
    public void setActive(boolean active) {
        this.active = active;
    }
}