package Dto;

/***
 * @author Alexandre Halleux
 * @version 1.0.1
 */

import java.util.ArrayList;
import java.util.Date;

public class OrderDto {


    private int id;
    private Date dateOrder;
    private ArrayList<ProductDto> listProduct;
    private ArrayList<MenuDto> listMenu;
    private double price;

    /***
     * No parametric constructor
     */
    public OrderDto(){
        listProduct = new ArrayList<>();
    }

    /***
     * Parametric constructor
     * @param id
     * @param dateOrder
     * @param listProduct
     * @param price
     */
    public OrderDto(int id, Date dateOrder, ArrayList<ProductDto> listProduct, ArrayList<MenuDto> listMenu, double price) {
        this.id = id;
        this.dateOrder = dateOrder;
        this.listProduct = listProduct;
        this.listMenu = listMenu;
        this.price = price;
    }

    /***
     * Getter id OrderDto
     * @return id orderDto
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id orderDto
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter Date orderDto
     * @return date orderDto
     */
    public Date getDateOrder() {
        return dateOrder;
    }

    /***
     * Setter Date orderDto
     * @param dateOrder
     */
    public void setDateOrder(Date dateOrder) {
        this.dateOrder = dateOrder;
    }

    /***
     * Getter list product in OrderDto
     * @return list product in OrderDto
     */
    public ArrayList<ProductDto> getListProduct() {
        return listProduct;
    }

    /***
     * Setter list product in OrderDto
     * @param listProduct
     */
    public void setListProduct(ArrayList<ProductDto> listProduct) {
        this.listProduct = listProduct;
    }

    /***
     * Getter list menu in OrderDto
     * @return list menu in OrderDto
     */
    public ArrayList<MenuDto> getListMenu() {
        return listMenu;
    }

    /***
     * Setter list menu in OrderDto
     * @param listMenu
     */
    public void setListMenu(ArrayList<MenuDto> listMenu) {
        this.listMenu = listMenu;
    }

    /***
     * Getter price orderDto
     * @return price orderDto
     */
    public double getPrice() {
        return price;
    }

    /***
     * Setter price orderDto
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }
}