package Dto;

/***
 * @author Alexandre Halleux
 * @version 1.0.1
 */

import java.util.ArrayList;

public class TeamDto {

    private int id;
    private String label;
    ArrayList<PersonDto> listPerson;

    /***
     * No parametric constructor
     */
    public TeamDto(){
        listPerson = new ArrayList<>();
    }

    /***
     * Parametric constructor
     * @param id
     * @param label
     * @param listPerson
     */
    public TeamDto(int id, String label, ArrayList<PersonDto> listPerson) {
        this.id = id;
        this.label = label;
        this.listPerson = listPerson;
    }

    /***
     * Getter Id Team
     * @return id Team
     */
    public int getId() {
        return id;
    }

    /***
     * Setter Id Team
     * @param id : identity Team
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter label Team
     * @return label Team
     */
    public String getLabel() {
        return label;
    }

    /***
     * Setter label Team
     * @param label : label Team
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /***
     * Getter list of Person
     * @return List of Person
     */
    public ArrayList<PersonDto> getListPerson() {
        return listPerson;
    }

    /***
     * Setter list of Person
     * @param listPerson : List of Person
     */
    public void setListPerson(ArrayList<PersonDto> listPerson) {
        this.listPerson = listPerson;
    }
}
