package Dto;

/***
 * @author Alexandre Halleux
 * @version 1.0.1
 */

public class ProductDto {

    private int id;
    private String name;
    private String category;
    private double price;
    private double priceOffer;
    private boolean specialOffer;
    private boolean menu;

    /***
     * No parametric constructor
     */
    public ProductDto(){}

    /***
     * Parametric constructor
     * @param id
     * @param name
     * @param category
     * @param price
     * @param menu
     */
    public ProductDto(int id, String name, String category, double price, double priceOffer, boolean specialOffer, boolean menu) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.priceOffer = priceOffer;
        this.specialOffer = specialOffer;
        this.menu = menu;
    }

    /***
     * Getter id productDto
     * @return id productDto
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id productDto
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter name productDto
     * @return name productDto
     */
    public String getName() {
        return name;
    }

    /***
     * Setter name productDto
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * Getter Category productDto
     * @return category productDto
     */
    public String getCategory() {
        return category;
    }

    /***
     * Setter category productDto
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /***
     * Getter price productDto
     * @return price productDto
     */
    public double getPrice() {
        return price;
    }

    /***
     * Setter price productDto
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /***
     * Getter priceOffer productDto
     * @return priceOffer productDto
     */
    public double getPriceOffer() {
        return priceOffer;
    }

    /***
     * Setter priceOffer productDto
     * @param priceOffer
     */
    public void setPriceOffer(double priceOffer) {
        this.priceOffer = priceOffer;
    }

    /***
     * Getter specialOffer productDto
     * @return specialOffer productDto
     */
    public boolean isSpecialOffer() {
        return specialOffer;
    }

    /***
     * Setter specialOffer productDto
     * @param specialOffer
     */
    public void setSpecialOffer(boolean specialOffer) {
        this.specialOffer = specialOffer;
    }

    /***
     * Getter menu productDto
     * @return menu productDto
     */
    public boolean isMenu() {
        return menu;
    }

    /***
     * Setter menu productDto
     * @param menu
     */
    public void setMenu(boolean menu) {
        this.menu = menu;
    }

}
