package Process;

/***
 * @author Alessandro Cecotti
 * @version 1.0.2
 */

import Models.*;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class CreateJson {

    private static final String PATH = System.getProperty("user.dir");

    private CreateJson(){
        // never call
    }

    /***
     * Create a json file with all information of Person
     * @param person
     */
    public static void jsonPerson(Person person){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", person.getId());
        jsonObject.put("LastName", person.getLastname());
        jsonObject.put("FirstName", person.getFirstname());
        jsonObject.put("Role", person.getRole());
        jsonObject.put("Login", person.getLogin());
        jsonObject.put("Password", person.getPassword());
        jsonObject.put("Active", person.isActive());


        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\person.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of Team
     * @param team
     */
    public static void jsonTeam(Team team){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", team.getId());
        jsonObject.put("Label", team.getLabel());
        jsonObject.put("ListPerson", team.getListPerson());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\team.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of Order
     * @param order
     */
    public static void jsonOrder(Order order){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", order.getId());
        jsonObject.put("Date", order.getDateOrder());
        jsonObject.put("ListProduct", order.getListProduct());
        jsonObject.put("ListMenu", order.getListMenu());
        jsonObject.put("Price", order.getPrice() + "€");

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\order.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of Menu
     * @param menu
     */
    public static void jsonMenu(Menu menu){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", menu.getId());
        jsonObject.put("Name", menu.getName());
        jsonObject.put("Products", menu.getListProduct());
        jsonObject.put("Price", menu.getPrice());
        jsonObject.put("PriceOffer", menu.getPriceOffer());
        jsonObject.put("SpecialOffer", menu.isSpecialOffer());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\menu.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of Product
     * @param product
     */
    public static void jsonProduct(Product product){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", product.getId());
        jsonObject.put("Name", product.getName());
        jsonObject.put("Category", product.getCategory());
        jsonObject.put("Price", product.getPrice());
        jsonObject.put("PriceOffer", product.getPriceOffer());
        jsonObject.put("SpecialOffer", product.isSpecialOffer());
        jsonObject.put("Menu",product.isMenu());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\product.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
