package Process;

import Models.Category;
import Models.Product;
import Utils.StringUtils;

/***
 * @author Grégory Viandier
 * @version 1.0.2
 */

public class ManageProduct {

    /***
     * Change the category of a product
     * @param product
     * @param category
     * @return product
     */
    public static Product changeCategory (Product product, String category){
        product.setCategory(Category.valueOf(category));

        return product;
    }

    /***
     * Update the price and priceoffer of a product
     * @param product
     * @param price
     * @param priceOffer
     * @return product
     */
    public static Product updatePrices (Product product, String price, String priceOffer){
        product.setPrice(StringUtils.stringToDouble(price));
        product.setPriceOffer(StringUtils.stringToDouble(priceOffer));

        return product;
    }

    /***
     * Change the state of a special offer
     * @param product
     * @return product
     */
    public static Product changeSpecialOffer(Product product){
        product.setSpecialOffer(product.isSpecialOffer() ? false : true);

        return product;
    }

    /***
     * Change the state of a menu
     * @param product
     * @return product
     */
    public static Product changeMenu(Product product){
        product.setMenu(product.isMenu() ? false : true);

        return product;
    }


}
