package Process;

/***
 * @author Alexandre Halleux
 * @version 1.0.2
 */

import Models.Menu;
import Models.Order;
import Models.Product;

import java.util.ArrayList;
import java.util.Date;

public class ManageOrder {

    /***
     * Calculate the price in a order
     * @param order
     * @return Order
     */
    public static Order calculatePrices(Order order){
        double price = 0;

        for(Product product : order.getListProduct()){
            if(product.isSpecialOffer()){
                price += product.getPriceOffer();
            }else{
                price += product.getPrice();
            }
        }

        for(Menu menu : order.getListMenu()){
            if(menu.isSpecialOffer()){
                price += menu.getPriceOffer();
            }else{
                price += menu.getPrice();
            }
        }

        order.setPrice(price);

        return order;
    }

    /***
     * Add 1 or infinite products in a order
     * @param order
     * @param products
     * @return order
     */
    public static Order addProduct(Order order, Product... products){
        ArrayList<Product> listProduct = new ArrayList<>();

        for(Product product : products){
            listProduct.add(product);
        }

        order.setListProduct(listProduct);
        order = calculatePrices(order);

        return order;
    }

    /***
     * Add 1 or infinite menus in a order
     * @param order
     * @param menus
     * @return order
     */
    public static Order addMenu(Order order, Menu... menus){
        ArrayList<Menu> listMenu = new ArrayList<>();

        for(Menu menu : menus){
            listMenu.add(menu);
        }

        order.setListMenu(listMenu);
        order = calculatePrices(order);

        return order;
    }

    /***
     * Remove 1 or infinite products in a menu
     * @param order
     * @param products
     * @return order
     */
    public static Order removeProduct(Order order, Product... products){
        ArrayList<Product> listProduct = order.getListProduct();

        for(Product product : products){
            if(listProduct.contains(product)){
                listProduct.remove(product);
            }
        }

        order.setListProduct(listProduct);
        order = calculatePrices(order);

        return order;
    }

    /***
     * Remove 1 or infinite menus in a menu
     * @param order
     * @param menus
     * @return order
     */
    public static Order removeMenu(Order order, Menu... menus){
        ArrayList<Menu> listMenu = order.getListMenu();

        for(Menu menu : menus){
            if(listMenu.contains(menu)){
                listMenu.remove(menu);
            }
        }

        order.setListMenu(listMenu);
        order = calculatePrices(order);

        return order;
    }

    /***
     * Put the today date
     * @param order
     * @return
     */
    public static Order today (Order order){

        order.setDateOrder(new Date());

        return order;
    }

}
