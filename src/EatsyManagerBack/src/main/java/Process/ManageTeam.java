package Process;

import Models.Person;
import Models.Product;
import Models.Team;

import java.util.ArrayList;

/***
 * @author Quentin Gallois
 * @version 1.0.2
 */

public class ManageTeam {

    /***
     * Add person or people in a team
     * @param team
     * @param people
     * @return team
     */
    public static Team addPerson (Team team, Person... people){
        ArrayList<Person> listPerson = new ArrayList<>();

        for(Person person : people){
            listPerson.add(person);
        }

        team.setListPerson(listPerson);

        return team;
    }

    /***
     * Remove person or people in a team
     * @param team
     * @param people
     * @return team
     */
    public static Team removePerson (Team team, Person... people){
        ArrayList<Person> listPerson = team.getListPerson();

        for(Person person : people){
            if(listPerson.contains(person)){
                listPerson.remove(person);
            }
        }

        team.setListPerson(listPerson);

        return team;
    }

}
