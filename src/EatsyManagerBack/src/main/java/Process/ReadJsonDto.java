package Process;

import Dto.*;
import Utils.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/***
 * @author Alessandro Cecotti
 * @version 1.0.2
 */

public class ReadJsonDto {

    private static final String PATH = System.getProperty("user.dir");

    /***
     * Constructor
     */
    private ReadJsonDto(){
        //never call
    }

    /***
     * Create personDto with information in a json file
     * @return person
     */
    public static PersonDto readjsonPersonDto(){
        File file = new File(PATH + "\\filejson\\personDto.json");
        PersonDto person = new PersonDto();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\personDto.json"));
                person.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                person.setLastname(jsonObject.get("LastName").toString());
                person.setFirstname(jsonObject.get("FirstName").toString());
                person.setRole(jsonObject.get("Role").toString());
                person.setLogin(jsonObject.get("Login").toString());
                person.setPassword(jsonObject.get("Password").toString());
                person.setActive(StringUtils.stringToBoolean(jsonObject.get("Active").toString()));

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return person;
    }

    /***
     * Create teamDto with information in a json file
     * @return team
     */
    public static TeamDto readjsonTeamDto(){
        File file = new File(PATH + "\\filejson\\teamDto.json");
        TeamDto team = new TeamDto();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\teamDto.json"));
                team.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                team.setLabel(jsonObject.get("Label").toString());

                JSONArray jsonArray = (JSONArray) jsonObject.get("ListPerson");
                Iterator<String> iterator = jsonArray.iterator();
                while (iterator.hasNext()){
                    PersonDto person = new PersonDto();
                }

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return team;
    }

    /***
     * Create orderDto with information in a json file
     * @return order
     */
    public static OrderDto readjsonOrderDto(){
        File file = new File(PATH + "\\filejson\\orderDto.json");
        OrderDto order = new OrderDto();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\orderDto.json"));
                order.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                order.setDateOrder(StringUtils.stringToDate(jsonObject.get("Date").toString()));
                order.setPrice(StringUtils.stringToDouble(jsonObject.get("Price").toString()));

                JSONArray jsonArrayMenu = (JSONArray) jsonObject.get("ListMenu");
                Iterator<String> iteratorMenu = jsonArrayMenu.iterator();
                while (iteratorMenu.hasNext()){
                    MenuDto menu = new MenuDto();
                }

                JSONArray jsonArrayProduct = (JSONArray) jsonObject.get("ListProduct");
                Iterator<String> iteratorProduct = jsonArrayProduct.iterator();
                while (iteratorProduct.hasNext()){
                    ProductDto product = new ProductDto();
                }

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return order;
    }

    /***
     * Create menuDto with information in a json file
     * @return menu
     */
    public static MenuDto readjsonMenuDto(){
        File file = new File(PATH + "\\filejson\\menuDto.json");
        MenuDto menu = new MenuDto();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\menuDto.json"));
                menu.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                menu.setName(jsonObject.get("Name").toString());
                menu.setPrice(StringUtils.stringToDouble(jsonObject.get("Price").toString()));
                menu.setPriceOffer(StringUtils.stringToDouble(jsonObject.get("PriceOffer").toString()));
                menu.setSpecialOffer(StringUtils.stringToBoolean(jsonObject.get("SpecialOffer").toString()));

                JSONArray jsonArrayProduct = (JSONArray) jsonObject.get("Products");
                Iterator<String> iteratorProduct = jsonArrayProduct.iterator();
                while (iteratorProduct.hasNext()){
                    ProductDto product = new ProductDto();
                }

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return menu;
    }

    /***
     * Create productDto with information in a json file
     * @return product
     */
    public static ProductDto readjsonProductDto(){
        File file = new File(PATH + "\\filejson\\productDto.json");
        ProductDto product = new ProductDto();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\productDto.json"));
                product.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                product.setName(jsonObject.get("Name").toString());
                product.setCategory(jsonObject.get("Category").toString());
                product.setPrice(StringUtils.stringToDouble(jsonObject.get("Price").toString()));
                product.setPriceOffer(StringUtils.stringToDouble(jsonObject.get("PriceOffer").toString()));
                product.setSpecialOffer(StringUtils.stringToBoolean(jsonObject.get("SpecialOffer").toString()));
                product.setMenu(StringUtils.stringToBoolean(jsonObject.get("Menu").toString()));

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return product;
    }
}
