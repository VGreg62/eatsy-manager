package Process;

import Models.*;
import Utils.StringUtils;
import org.json.simple.*;
import org.json.simple.parser.*;

import java.io.*;
import java.util.Iterator;

/***
 * @author Alessandro Cecotti
 * @version 1.0.2
 */

public class ReadJson {

    private static final String PATH = System.getProperty("user.dir");

    /***
     * Constructor
     */
    private ReadJson(){
        // never call
    }

    /***
     * Create person with information in a json file
     * @return person
     */
    public static Person readjsonPerson(){
        File file = new File(PATH + "\\filejson\\person.json");
        Person person = new Person();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\person.json"));
                person.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                person.setLastname(jsonObject.get("LastName").toString());
                person.setFirstname(jsonObject.get("FirstName").toString());
                person.setRole(Role.valueOf(jsonObject.get("Role").toString()));
                person.setLogin(jsonObject.get("Login").toString());
                person.setPassword(jsonObject.get("Password").toString());
                person.setActive(StringUtils.stringToBoolean(jsonObject.get("Active").toString()));

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return person;
    }

    /***
     * Create team with information in a json file
     * @return team
     */
    public static Team readjsonTeam(){
        File file = new File(PATH + "\\filejson\\team.json");
        Team team = new Team();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\team.json"));
                team.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                team.setLabel(jsonObject.get("Label").toString());

                JSONArray jsonArray = (JSONArray) jsonObject.get("ListPerson");
                Iterator<String> iterator = jsonArray.iterator();
                while (iterator.hasNext()){
                    Person person = new Person();
                }

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return team;
    }

    /***
     * Create order with information in a json file
     * @return order
     */
    public static Order readjsonOrder(){
        File file = new File(PATH + "\\filejson\\order.json");
        Order order = new Order();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\order.json"));
                order.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                order.setDateOrder(StringUtils.stringToDate(jsonObject.get("Date").toString()));
                order.setPrice(StringUtils.stringToDouble(jsonObject.get("Price").toString()));

                JSONArray jsonArrayMenu = (JSONArray) jsonObject.get("ListMenu");
                Iterator<String> iteratorMenu = jsonArrayMenu.iterator();
                while (iteratorMenu.hasNext()){
                    Menu menu = new Menu();
                }

                JSONArray jsonArrayProduct = (JSONArray) jsonObject.get("ListProduct");
                Iterator<String> iteratorProduct = jsonArrayProduct.iterator();
                while (iteratorProduct.hasNext()){
                    Product product = new Product();
                }

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return order;
    }

    /***
     * Create menu with information in a json file
     * @return menu
     */
    public static Menu readjsonMenu(){
        File file = new File(PATH + "\\filejson\\menu.json");
        Menu menu = new Menu();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\menu.json"));
                menu.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                menu.setName(jsonObject.get("Name").toString());
                menu.setPrice(StringUtils.stringToDouble(jsonObject.get("Price").toString()));
                menu.setPriceOffer(StringUtils.stringToDouble(jsonObject.get("PriceOffer").toString()));
                menu.setSpecialOffer(StringUtils.stringToBoolean(jsonObject.get("SpecialOffer").toString()));

                JSONArray jsonArrayProduct = (JSONArray) jsonObject.get("Products");
                Iterator<String> iteratorProduct = jsonArrayProduct.iterator();
                while (iteratorProduct.hasNext()){
                    Product product = new Product();
                }

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return menu;
    }

    /***
     * Create product with information in a json file
     * @return product
     */
    public static Product readjsonProduct(){
        File file = new File(PATH + "\\filejson\\product.json");
        Product product = new Product();

        if(file.exists()){
            try {
                JSONParser jsonParser = new JSONParser();

                //Parsing the contents of the JSON file
                JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(PATH + "\\filejson\\product.json"));
                product.setId(StringUtils.stringToInt(jsonObject.get("ID").toString()));
                product.setName(jsonObject.get("Name").toString());
                product.setCategory(Category.valueOf(jsonObject.get("Category").toString()));
                product.setPrice(StringUtils.stringToDouble(jsonObject.get("Price").toString()));
                product.setPriceOffer(StringUtils.stringToDouble(jsonObject.get("PriceOffer").toString()));
                product.setSpecialOffer(StringUtils.stringToBoolean(jsonObject.get("SpecialOffer").toString()));
                product.setMenu(StringUtils.stringToBoolean(jsonObject.get("Menu").toString()));

            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ParseException e){
                e.printStackTrace();
            }

        }else{
            return null;
        }

        return product;
    }
}
