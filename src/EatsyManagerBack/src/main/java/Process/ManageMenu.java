package Process;

/***
 * @author Alexandre Halleux
 * @version 1.0.2
 */

import Models.Menu;
import Models.Product;

import java.util.ArrayList;

public class ManageMenu {

    /***
     * Calculate the price and priceOffer in a menu
     * @param menu
     * @return menu
     */
    public static Menu calculatePrices(Menu menu){
        double price = 0;
        double priceOffer = 0;

        for(Product product : menu.getListProduct()){
            price += product.getPrice();
            priceOffer += product.getPriceOffer();
        }

        menu.setPrice(price);
        menu.setPriceOffer(priceOffer);

        return menu;
    }

    /***
     * Add 1 or infinite products in a menu
     * @param menu
     * @param products
     * @return menu
     */
    public static Menu addProduct(Menu menu, Product... products){
        ArrayList<Product> listProduct = new ArrayList<>();

        for(Product product : products){
            listProduct.add(product);
        }

        menu.setListProduct(listProduct);
        menu = calculatePrices(menu);

        return menu;
    }

    /***
     * Remove 1 or infinite products in a menu
     * @param menu
     * @param products
     * @return menu
     */
    public static Menu removeProduct(Menu menu, Product... products){
        ArrayList<Product> listProduct = menu.getListProduct();

        for(Product product : products){
            if(listProduct.contains(product)){
                listProduct.remove(product);
            }
        }

        menu.setListProduct(listProduct);
        menu = calculatePrices(menu);

        return menu;
    }

    /***
     * Change the state of the specialOffer
     * @param menu
     * @return
     */
    public static Menu changeSpecialOffer(Menu menu){

        //If menu.isSpecialOffer is true return false and if is false return true
        menu.setSpecialOffer(menu.isSpecialOffer() ? false : true);

        return menu;
    }

}
