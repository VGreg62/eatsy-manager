package Process;

/***
 * @author Grégory Viandier
 * @version 1.0.2
 */


import Models.Person;
import Models.Role;
import Utils.Hash;

public class ManagePerson {

    /***
     * Change the password of a person
     * @param person
     * @param password
     * @return person
     */
    public static Person changePassword(Person person, String password){
        person.setPassword(Hash.hashPassword(password));

        return person;
    }

    /***
     * Change the login of a person
     * @param person
     * @param login
     * @return person
     */
    public static Person changeLogin(Person person, String login){
        person.setLogin(login);

        return person;
    }

    /***
     * Change the role of a person
     * @param person
     * @param role
     * @return person
     */
    public static Person changeRole(Person person, String role){
        person.setRole(Role.valueOf(role));

        return person;
    }

    /***
     * Change the state of a person
     * @param person
     * @return person
     */
    public static Person changeActive (Person person){
        person.setActive(person.isActive() ? false : true);

        return person;
    }

}
