package Process;

/***
 * @author Alessandro Cecotti
 * @version 1.0.2
 */

import Dto.*;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class CreateJsonDto {

    private static final String PATH = System.getProperty("user.dir");

    private CreateJsonDto(){
        // never call
    }

    /***
     * Create a json file with all information of PersonDto
     * @param personDto
     */
    public static void jsonPersonDto(PersonDto personDto){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", personDto.getId());
        jsonObject.put("LastName", personDto.getLastname());
        jsonObject.put("FirstName", personDto.getFirstname());
        jsonObject.put("Role", personDto.getRole());
        jsonObject.put("Login", personDto.getLogin());
        jsonObject.put("Password", personDto.getPassword());
        jsonObject.put("Active", personDto.isActive());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\personDto.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of TeamDto
     * @param teamDto
     */
    public static void jsonTeamDto(TeamDto teamDto){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", teamDto.getId());
        jsonObject.put("Label", teamDto.getLabel());
        jsonObject.put("ListPerson", teamDto.getListPerson());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\teamDto.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of OrderDto
     * @param orderDto
     */
    public static void jsonOrderDto(OrderDto orderDto){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", orderDto.getId());
        jsonObject.put("Date", orderDto.getDateOrder());
        jsonObject.put("ListProduct", orderDto.getListProduct());
        jsonObject.put("ListMenu", orderDto.getListMenu());
        jsonObject.put("Price", orderDto.getPrice() + "€");

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\orderDto.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of MenuDto
     * @param menuDto
     */
    public static void jsonMenuDto(MenuDto menuDto){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", menuDto.getId());
        jsonObject.put("Name", menuDto.getName());
        jsonObject.put("Products", menuDto.getListProduct());
        jsonObject.put("Price", menuDto.getPrice());
        jsonObject.put("PriceOffer", menuDto.getPriceOffer());
        jsonObject.put("SpecialOffer", menuDto.isSpecialOffer());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\menuDto.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /***
     * Create a json file with all information of ProductDto
     * @param productDto
     */
    public static void jsonProductDto(ProductDto productDto){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("ID", productDto.getId());
        jsonObject.put("Name", productDto.getName());
        jsonObject.put("Category", productDto.getCategory());
        jsonObject.put("Price", productDto.getPrice());
        jsonObject.put("PriceOffer", productDto.getPriceOffer());
        jsonObject.put("SpecialOffer", productDto.isSpecialOffer());
        jsonObject.put("Menu",productDto.isMenu());

        try{
            FileWriter file = new FileWriter(PATH + "\\filejson\\productDto.json");
            file.write(jsonObject.toString());
            file.flush();
            file.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
