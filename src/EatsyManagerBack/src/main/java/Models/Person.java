package Models;

/***
 * @author Alessandro Cecotti
 * @version 1.0
 */

import Utils.Hash;

/***
 * @author Grégory Viandier
 * @version 1.0.1
 */

public class Person {

    private int id;
    private String lastname;
    private String firstname;
    private Role role;
    private String login;
    private String password;
    private boolean active;

    /***
     * No parametric constructor
     */
    public Person(){}

    /***
     * Parametric Constructor
     * @param id
     * @param lastname
     * @param firstname
     * @param role
     * @param login
     * @param password
     * @param active
     */
    public Person(int id, String lastname, String firstname, Role role, String login, String password, boolean active) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.role = role;
        this.login = login;
        this.password = Hash.hashPassword(password);
        this.active = active;
    }

    /***
     * Getter id person
     * @return id person
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id person
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter lastname person
     * @return lastname person
     */
    public String getLastname() {
        return lastname;
    }

    /***
     * Setter lastname person
     * @param lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /***
     * Getter firstname person
     * @return firstname person
     */
    public String getFirstname() {
        return firstname;
    }

    /***
     * Setter firstname person
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /***
     * Getter role person
     * @return role person
     */
    public Role getRole() {
        return role;
    }

    /***
     * Setter role person
     * @param role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /***
     * Getter login person
     * @return login person
     */
    public String getLogin() {
        return login;
    }

    /***
     * Setter login person
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /***
     * Getter password person
     * @return password person
     */
    public String getPassword() {
        return password;
    }

    /***
     * Setter password person
     * @param password
     */
    public void setPassword(String password) {
        this.password = Hash.hashPassword(password);
    }

    /***
     * Getter active person
     * @return active person
     */
    public boolean isActive() {
        return active;
    }

    /***
     * Setter active person
     * @param active
     */
    public void setActive(boolean active) {
        this.active = active;
    }
}
