package Models;

/***
 * @author Alessandro Cecotti
 * @version 1.0.1
 */

public enum Category {

    ENTREE("Entrée"),
    PLAT_PRINCIPAL("Plat principal"),
    BOISSON_SOFT("Boisson soft"),
    BOISSON_ALCOOL("Boisson alcoolisé"),
    ACCOMPAGNEMENT("Accompagnement"),
    DESSERT("Dessert"),
    FROMAGE("Fromage");

    private String name;

    /***
     * Constructor
     * @param name
     */
    Category(String name) {
        this.name = name;
    }

    /***
     * Getter name to Enum Category
     * @return String name
     */
    public String getName(){
        return name;
    }

}
