package Models;

/***
 * @author Alessandro Cecotti
 * @version 1.0.1
 */

public class Product {

    private int id;
    private String name;
    private Category category;
    private double price;
    private double priceOffer;
    private boolean specialOffer;
    private boolean menu;

    /***
     * No parametric constructor
     */
    public Product(){}

    /***
     * Parametric constructor
     * @param id
     * @param name
     * @param category
     * @param price
     * @param menu
     */
    public Product(int id, String name, Category category, double price, double priceOffer, boolean specialOffer, boolean menu) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.priceOffer = priceOffer;
        this.specialOffer = specialOffer;
        this.menu = menu;
    }

    /***
     * Getter id product
     * @return id product
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id product
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter name product
     * @return name product
     */
    public String getName() {
        return name;
    }

    /***
     * Setter name product
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * Getter Category product
     * @return category product
     */
    public Category getCategory() {
        return category;
    }

    /***
     * Setter category product
     * @param category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /***
     * Getter price product
     * @return price product
     */
    public double getPrice() {
        return price;
    }

    /***
     * Setter price product
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /***
     * Getter priceOffer product
     * @return priceOffer product
     */
    public double getPriceOffer() {
        return priceOffer;
    }

    /***
     * Setter priceOffer product
     * @param priceOffer
     */
    public void setPriceOffer(double priceOffer) {
        this.priceOffer = priceOffer;
    }

    /***
     * Getter specialOffer product
     * @return specialOffer product
     */
    public boolean isSpecialOffer() {
        return specialOffer;
    }

    /***
     * Setter specialOffer product
     * @param specialOffer
     */
    public void setSpecialOffer(boolean specialOffer) {
        this.specialOffer = specialOffer;
    }

    /***
     * Getter menu product
     * @return menu product
     */
    public boolean isMenu() {
        return menu;
    }

    /***
     * Setter menu product
     * @param menu
     */
    public void setMenu(boolean menu) {
        this.menu = menu;
    }

}
