package Models;

/***
 * @author Alessandro Cecotti
 * @version 1.0.1
 */

import java.util.ArrayList;
import java.util.Date;

public class Order {

    private int id;
    private Date dateOrder;
    private ArrayList<Product> listProduct;
    private ArrayList<Menu> listMenu;
    private double price;

    /***
     * No parametric constructor
     */
    public Order(){
        listProduct = new ArrayList<>();
    }

    /***
     * Parametric constructor
     * @param id
     * @param dateOrder
     * @param listProduct
     * @param price
     */
    public Order(int id, Date dateOrder, ArrayList<Product> listProduct, ArrayList<Menu> listMenu, double price) {
        this.id = id;
        this.dateOrder = dateOrder;
        this.listProduct = listProduct;
        this.listMenu = listMenu;
        this.price = price;
    }

    /***
     * Getter id Order
     * @return id order
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id order
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter Date order
     * @return date order
     */
    public Date getDateOrder() {
        return dateOrder;
    }

    /***
     * Setter Date order
     * @param dateOrder
     */
    public void setDateOrder(Date dateOrder) {
        this.dateOrder = dateOrder;
    }

    /***
     * Getter list product in Order
     * @return list product in Order
     */
    public ArrayList<Product> getListProduct() {
        return listProduct;
    }

    /***
     * Setter list product in Order
     * @param listProduct
     */
    public void setListProduct(ArrayList<Product> listProduct) {
        this.listProduct = listProduct;
    }

    /***
     * Getter list menu in Order
     * @return list menu in Order
     */
    public ArrayList<Menu> getListMenu() {
        return listMenu;
    }

    /***
     * Setter list menu in Order
     * @param listMenu
     */
    public void setListMenu(ArrayList<Menu> listMenu) {
        this.listMenu = listMenu;
    }

    /***
     * Getter price order
     * @return price order
     */
    public double getPrice() {
        return price;
    }

    /***
     * Setter price order
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }
}
