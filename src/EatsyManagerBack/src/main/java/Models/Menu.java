package Models;

/***
 * @author Alessandro Cecotti
 * @version 1.0.1
 */

import java.util.ArrayList;

public class Menu {

    private int id;
    private String name;
    private ArrayList<Product> listProduct;
    private double price;
    private double priceOffer;
    private boolean specialOffer;

    /***
     * No parametric constructor
     */
    public Menu(){
        listProduct = new ArrayList<>();
    }

    /***
     * Parametric constructor
     * @param name
     * @param listProduct
     * @param price
     * @param priceOffer
     * @param specialOffer
     */
    public Menu( String name, ArrayList<Product> listProduct, double price, double priceOffer, boolean specialOffer) {
        this.name = name;
        this.listProduct = listProduct;
        this.price = price;
        this.priceOffer = priceOffer;
        this.specialOffer = specialOffer;
    }

    /***
     * Parametric constructor
     * @param id
     * @param name
     * @param listProduct
     * @param price
     */
    public Menu(int id, String name, ArrayList<Product> listProduct, double price, double priceOffer, boolean specialOffer) {
        this.id = id;
        this.name = name;
        this.listProduct = listProduct;
        this.price = price;
        this.priceOffer = priceOffer;
        this.specialOffer = specialOffer;
    }

    /***
     * Getter id product
     * @return id product
     */
    public int getId() {
        return id;
    }

    /***
     * Setter id product
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /***
     * Getter name product
     * @return name product
     */
    public String getName() {
        return name;
    }

    /***
     * Setter name product
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * Getter ListProduct in menu
     * @return ListProduct in menu
     */
    public ArrayList<Product> getListProduct() {
        return listProduct;
    }

    /***
     * Setter ListProduct in menu
     * @param listProduct
     */
    public void setListProduct(ArrayList<Product> listProduct) {
        this.listProduct = listProduct;
    }

    /***
     * Getter price product
     * @return price product
     */
    public double getPrice() {
        return price;
    }

    /***
     * Setter price product
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /***
     * Getter priceOffer product
     * @return priceOffer product
     */
    public double getPriceOffer() {
        return priceOffer;
    }

    /***
     * Setter priceOffer product
     * @param priceOffer
     */
    public void setPriceOffer(double priceOffer) {
        this.priceOffer = priceOffer;
    }

    /***
     * Getter specialOffer product
     * @return specialOffer product
     */
    public boolean isSpecialOffer() {
        return specialOffer;
    }

    /***
     * Setter specialOffer product
     * @param specialOffer
     */
    public void setSpecialOffer(boolean specialOffer) {
        this.specialOffer = specialOffer;
    }

}
