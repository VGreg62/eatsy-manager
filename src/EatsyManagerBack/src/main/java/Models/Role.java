package Models;

/***
 * @author Alessandro Cecotti
 * @version 1.0.1
 */

/***
 * Contains all the roles of a restaurant
 */
public enum Role {

    CHEF_CUISINIER("Chef_cuisinier"),
    SERVEUR("Serveur"),
    DIRECTEUR("Directeur"),
    SECOND("Second"),
    COMMIS("Commis"),
    PLONGEUR("Plongeur"),
    CAISSIER("Caissier"),
    SAUCIER("Saucier");

    private String name;

    /***
     * Constructor
     * @param name
     */
    Role(String name) {
        this.name = name;
    }

    /***
     * Getter name to Enum Role
     * @return String name
     */
    public String getName(){
        return name;
    }
}
