package Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * @author Grégory Viandier
 * @version 1.0
 */

public class StringUtils {

    private StringUtils(){
        //never called
    }

    /***
     * Transform int to String
     * @param number
     * @return String
     */
    public static String intToString(int number){
        return String.valueOf(number);
    }

    /***
     * Transform double to String
     * @param number
     * @return String
     */
    public static String doubleToString(double number){
        return String.valueOf(number);
    }

    /***
     * Transform String to int
     * @param number
     * @return int
     */
    public static int stringToInt(String number){
        return Comparator.isInt(number) ? Integer.parseInt(number) : 0;
    }

    /***
     * Transform String to double
     * @param number
     * @return double
     */
    public static double stringToDouble(String number){
        return Comparator.isDouble(number) ? Double.parseDouble(number) : 0;
    }

    /***
     * Transform String to boolean
     * @param value
     * @return boolean
     */
    public static boolean stringToBoolean(String value){
        return Boolean.parseBoolean(value);
    }

    /***
     * Transform String to Date
     * @param sDate
     * @return Date
     */
    public static Date stringToDate(String sDate) {
        Date date = null;
        try{
            date = new SimpleDateFormat("DD/MM/yyyy").parse(sDate);
        }catch(ParseException e){
            e.printStackTrace();
        }

        return date;
    }
}
