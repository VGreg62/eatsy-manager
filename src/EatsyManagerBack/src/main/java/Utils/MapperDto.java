package Utils;

/***
 * @author Grégory Viandier
 * @version 1.0
 */

import Dto.*;
import Models.*;

import java.util.ArrayList;

public class MapperDto {

    /***
     * Map MenuDto to Menu
     * @param menuDto
     * @return Object Menu
     */
    public static Menu menuDtoToMenu(MenuDto menuDto){
        Menu menu = new Menu(menuDto.getId(), menuDto.getName(), null, menuDto.getPrice(), menuDto.getPriceOffer(), menuDto.isSpecialOffer());
        ArrayList<Product> listProduct = new ArrayList<>();

        for(ProductDto productDto : menuDto.getListProduct()){
            listProduct.add(productDtoToProduct(productDto));
        }

        menu.setListProduct(listProduct);

        return menu;
    }

    /***
     * Map OrderDto to Order
     * @param orderDto
     * @return Object Order
     */
    public static Order orderDtoToOrder(OrderDto orderDto){
        Order order = new Order(orderDto.getId(), orderDto.getDateOrder(), null, null, orderDto.getPrice());
        ArrayList<Product> listProduct = new ArrayList<>();
        ArrayList<Menu> listMenu = new ArrayList<>();

        for(ProductDto productDto : orderDto.getListProduct()){
            listProduct.add(productDtoToProduct(productDto));
        }

        for(MenuDto menuDto : orderDto.getListMenu()){
            listMenu.add(menuDtoToMenu(menuDto));
        }

        order.setListProduct(listProduct);
        order.setListMenu(listMenu);

        return order;
    }

    /***
     * Map ProductDto to Product
     * @param productDto
     * @return Object Product
     */
    public static Product productDtoToProduct(ProductDto productDto){
        return new Product(productDto.getId(), productDto.getName(), Category.valueOf(productDto.getCategory()), productDto.getPrice(), productDto.getPriceOffer(), productDto.isSpecialOffer(), productDto.isMenu());
    }

    /***
     * Map PersonDto to Person
     * @param personDto
     * @return Object Person
     */
    public static Person personDtoToPerson (PersonDto personDto){
        return new Person(personDto.getId(),personDto.getLastname(), personDto.getFirstname(), Role.valueOf(personDto.getRole()), personDto.getLogin(), personDto.getPassword(), personDto.isActive());
    }

    /***
     * Map TeamDto to Team
     * @param teamDto
     * @return Object Team
     */
    public static Team teamDtoToTeam (TeamDto teamDto){
        Team team = new Team(teamDto.getId(), teamDto.getLabel(), null);
        ArrayList<Person> listPerson = new ArrayList<>();

        for(PersonDto personDto : teamDto.getListPerson()){
            listPerson.add(personDtoToPerson(personDto));
        }

        return team;
    }

}
