package Utils;

/***
 * @author Grégory Viandier
 * @version 1.0
 */

import Dto.*;
import Models.*;

import java.util.ArrayList;

public class Mapper {

    /***
     * Map Menu to MenuDto
     * @param menu
     * @return Object MenuDto
     */
    public static MenuDto menuToMenuDto(Menu menu){
        MenuDto menuDto = new MenuDto(menu.getId(), menu.getName(), null, menu.getPrice(), menu.getPriceOffer(), menu.isSpecialOffer());
        ArrayList<ProductDto> listProductDto = new ArrayList<>();

        for(Product product : menu.getListProduct()){
            listProductDto.add(productToProductDto(product));
        }

        menuDto.setListProduct(listProductDto);

        return menuDto;
    }

    /***
     * Map Order to OrderDto
     * @param order
     * @return Object OrderDto
     */
    public static OrderDto orderToOrderDto(Order order){
        OrderDto orderDto = new OrderDto(order.getId(), order.getDateOrder(), null, null, order.getPrice());
        ArrayList<ProductDto> listProductDto = new ArrayList<>();
        ArrayList<MenuDto> listMenuDto = new ArrayList<>();

        for(Product product : order.getListProduct()){
            listProductDto.add(productToProductDto(product));
        }

        for(Menu menu : order.getListMenu()){
            listMenuDto.add(menuToMenuDto(menu));
        }

        orderDto.setListProduct(listProductDto);
        orderDto.setListMenu(listMenuDto);

        return orderDto;
    }

    /***
     * Map Product to ProductDto
     * @param product
     * @return Object ProductDto
     */
    public static ProductDto productToProductDto(Product product){
        return new ProductDto(product.getId(), product.getName(), product.getCategory().getName(), product.getPrice(), product.getPriceOffer(), product.isSpecialOffer(), product.isMenu());
    }

    /***
     * Map Person to PersonDto
     * @param person
     * @return Object PersonDto
     */
    public static PersonDto personToPersonDto (Person person){
        return new PersonDto(person.getId(),person.getLastname(), person.getFirstname(), person.getRole().getName(), person.getLogin(), person.getPassword(), person.isActive());
    }

    /***
     * Map Team to TeamDto
     * @param team
     * @return Object TeamDto
     */
    public static TeamDto teamToTeamDto (Team team){
        TeamDto teamDto = new TeamDto(team.getId(), team.getLabel(), null);
        ArrayList<PersonDto> listPersonDto = new ArrayList<>();

        for(Person person : team.getListPerson()){
            listPersonDto.add(personToPersonDto(person));
        }

        return teamDto;
    }

}
