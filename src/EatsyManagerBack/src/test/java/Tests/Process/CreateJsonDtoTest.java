package Tests.Process;

import Dto.*;
import Process.CreateJsonDto;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateJsonDtoTest {

    final String path = System.getProperty("user.dir");

    @Test
    void jsonPerson() {
        PersonDto person = new PersonDto(1, "Toto", "titi", "Directeur", "titiToto", "1234", false);

        CreateJsonDto.jsonPersonDto(person);

        File file = new File(path + "\\filejson\\personDto.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonTeam() {
        PersonDto person = new PersonDto(1, "Tata", "titi", "CHEF_CUISINIER", "titiToto", "1234", false);
        PersonDto person1 = new PersonDto(2, "Toto", "titi", "SECOND", "titiToto", "1234", false);
        PersonDto person2 = new PersonDto(3, "Tutu", "titi", "SAUCIER", "titiToto", "1234", false);

        ArrayList<PersonDto> listPerson = new ArrayList<PersonDto>(Arrays.asList(person, person1, person2));

        TeamDto team = new TeamDto(1, "Cuisine", listPerson);

        CreateJsonDto.jsonTeamDto(team);

        File file = new File(path + "\\filejson\\teamDto.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonProduct() {
        ProductDto product = new ProductDto(1, "Coca-cola", "BOISSON_SOFT", 2.3, 1.5, false, true);

        CreateJsonDto.jsonProductDto(product);

        File file = new File(path + "\\filejson\\productDto.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonMenu() {
        ProductDto product = new ProductDto(1, "Coca-cola", "BOISSON_SOFT", 2.3, 1.5, false, true);
        ProductDto product2 = new ProductDto(2, "Frite", "ACCOMPAGNEMENT", 1.5, 1, false, true);
        ProductDto product3 = new ProductDto(3, "Carbonade", "PLAT_PRINCIPAL", 14.5, 12.5, false, true);

        ArrayList<ProductDto> listProduct = new ArrayList<ProductDto>(Arrays.asList(product, product2, product3));

        MenuDto menu = new MenuDto(1,"Menu Flamant", listProduct, 18.3, 15, false);

        CreateJsonDto.jsonMenuDto(menu);

        File file = new File(path + "\\filejson\\menuDto.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonOrder() {
        ProductDto product = new ProductDto(1, "Coca-cola", "BOISSON_SOFT", 2.3, 1.5, false, true);
        ProductDto product2 = new ProductDto(2, "Frite", "ACCOMPAGNEMENT", 1.5, 1, false, true);
        ProductDto product3 = new ProductDto(3, "Carbonade", "PLAT_PRINCIPAL", 14.5, 12.5, false, true);
        ProductDto product4 = new ProductDto(4, "Steak", "PLAT_PRINCIPAL", 9.5, 8, true, false);

        ArrayList<ProductDto> listProduct = new ArrayList<ProductDto>(Arrays.asList(product, product2, product3));
        ArrayList<ProductDto> listProduct1 = new ArrayList<ProductDto>(Arrays.asList(product4));

        MenuDto menu = new MenuDto(1,"Menu Flamant", listProduct, 18.3, 15, false);
        ArrayList<MenuDto> listMenu = new ArrayList<>(Arrays.asList(menu));

        OrderDto order = new OrderDto(1,new Date(), listProduct1, listMenu, 26.3);

        CreateJsonDto.jsonOrderDto(order);

        File file = new File(path + "\\filejson\\orderDto.json");
        assertTrue(file.exists());
    }
}
