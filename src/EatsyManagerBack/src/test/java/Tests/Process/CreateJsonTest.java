package Tests.Process;

/***
 * @author Quentin Gallois
 * @version 1.0
 */

import Models.*;
import Process.CreateJson;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class CreateJsonTest {

    final String path = System.getProperty("user.dir");

    @Test
    void jsonPerson() {
        Person person = new Person(1, "Toto", "titi", Role.DIRECTEUR, "titiToto", "1234", false);

        CreateJson.jsonPerson(person);

        File file = new File(path + "\\filejson\\person.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonTeam() {
        Person person = new Person(1, "Tata", "titi", Role.CHEF_CUISINIER, "titiToto", "1234", false);
        Person person1 = new Person(2, "Toto", "titi", Role.SECOND, "titiToto", "1234", false);
        Person person2 = new Person(3, "Tutu", "titi", Role.SAUCIER, "titiToto", "1234", false);

        ArrayList<Person> listPerson = new ArrayList<Person>(Arrays.asList(person, person1, person2));

        Team team = new Team(1, "Cuisine", listPerson);

        CreateJson.jsonTeam(team);

        File file = new File(path + "\\filejson\\team.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonProduct() {
        Product product = new Product(1, "Coca-cola", Category.BOISSON_SOFT, 2.3, 1.5, false, true);

        CreateJson.jsonProduct(product);

        File file = new File(path + "\\filejson\\product.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonMenu() {
        Product product = new Product(1, "Coca-cola", Category.BOISSON_SOFT, 2.3, 1.5, false, true);
        Product product2 = new Product(2, "Frite", Category.ACCOMPAGNEMENT, 1.5, 1, false, true);
        Product product3 = new Product(3, "Carbonade", Category.PLAT_PRINCIPAL, 14.5, 12.5, false, true);

        ArrayList<Product> listProduct = new ArrayList<Product>(Arrays.asList(product, product2, product3));

        Menu menu = new Menu(1,"Menu Flamant", listProduct, 18.3, 15, false);

        CreateJson.jsonMenu(menu);

        File file = new File(path + "\\filejson\\menu.json");
        assertTrue(file.exists());
    }

    @Test
    void jsonOrder() {
        Product product = new Product(1, "Coca-cola", Category.BOISSON_SOFT, 2.3, 1.5, false, true);
        Product product2 = new Product(2, "Frite", Category.ACCOMPAGNEMENT, 1.5, 1, false, true);
        Product product3 = new Product(3, "Carbonade", Category.PLAT_PRINCIPAL, 14.5, 12.5, false, true);
        Product product4 = new Product(4, "Steak", Category.PLAT_PRINCIPAL, 9.5, 8, true, false);

        ArrayList<Product> listProduct = new ArrayList<Product>(Arrays.asList(product, product2, product3));
        ArrayList<Product> listProduct1 = new ArrayList<Product>(Arrays.asList(product4));

        Menu menu = new Menu(1,"Menu Flamant", listProduct, 18.3, 15, false);
        ArrayList<Menu> listMenu = new ArrayList<>(Arrays.asList(menu));

        Order order = new Order(1,new Date(), listProduct1, listMenu, 26.3);

        CreateJson.jsonOrder(order);

        File file = new File(path + "\\filejson\\order.json");
        assertTrue(file.exists());
    }


}
