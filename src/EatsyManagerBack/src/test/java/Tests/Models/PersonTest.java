package Tests.Models;

/***
 * @author Quentin Gallois
 * @version 1.0
 */

import Models.Person;
import Models.Role;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class PersonTest {

    @Test
    void construct(){
        Person person = new Person(1, "toto", "toto", Role.DIRECTEUR, "Ttoto", "1234", false);

        assertEquals("toto", person.getLastname());
        assertEquals("toto", person.getFirstname());
        assertEquals(Role.DIRECTEUR, person.getRole());
        assertEquals("Ttoto", person.getLogin());
        assertEquals("81dc9bdb52d04dc20036dbd8313ed055", person.getPassword());
        assertFalse(person.isActive());
    }
}
