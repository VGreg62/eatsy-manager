package Tests.Utils;

/***
 * @author Quentin Gallois
 * @version 1.0
 */

import Utils.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringUtilsTest {

    @Test
    void intToString() {
        int number = 2;

        assertEquals("2", StringUtils.intToString(number));
    }

    @Test
    void doubleToString() {
        double number = 2.2;

        assertEquals("2.2", StringUtils.doubleToString(number));
    }

    @Test
    void stringToInt() {
        String number = "2";

        assertEquals(2, StringUtils.stringToInt(number));
    }

    @Test
    void stringToDouble() {
        String number = "2.2";

        assertEquals(2.2, StringUtils.stringToDouble(number));
    }

    @Test
    void stringToIntFail() {
        String number = "A";

        assertEquals(0, StringUtils.stringToInt(number));
    }

    @Test
    void stringToDoubleFail() {
        String number = "A";

        assertEquals(0, StringUtils.stringToDouble(number));
    }

}
