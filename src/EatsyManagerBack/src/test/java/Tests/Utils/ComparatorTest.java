package Tests.Utils;

/***
 * @author Quentin Gallois
 * @version 1.0
 */

import Utils.Comparator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ComparatorTest {

    @Test
    void isInt() {
        String number = "2";

        assertTrue(Comparator.isInt(number));
    }

    @Test
    void isDouble() {
        String number = "2.2";

        assertTrue(Comparator.isDouble(number));
    }

    @Test
    void isIntFail() {
        String number = "A";

        assertFalse(Comparator.isInt(number));
    }

    @Test
    void isDoubleFail() {
        String number = "A";

        assertFalse(Comparator.isDouble(number));
    }

}
