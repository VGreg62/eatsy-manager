package Tests.Utils;

/***
 * @author Quentin Gallois
 * @version 1.0
 */

import Utils.Hash;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class HashTest {

    @Test
    public void hashPassword(){
        String password = "1234";
        String passwordhash = "81dc9bdb52d04dc20036dbd8313ed055";

        assertEquals(passwordhash,Hash.hashPassword(password));
    }

    @Test
    public void hashPasswordFail(){
        String password = "1234";
        String passwordhash = "1234";

        assertNotEquals(passwordhash,Hash.hashPassword(password));
    }
}
